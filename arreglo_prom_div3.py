# Calcular el promedio de los elementos del arreglo que son divisibles entre 3
# Javier Rivera
# Jandryth Torres

arreglo = [5,3,6,-8,5,7,-6,9,0,12,-1,15]
acum = 0
cont = 0

for i in arreglo:

	if (i % 3 == 0):
		
		acum = acum + i
		cont = cont + 1

print (" el promedio es ", acum / cont)

