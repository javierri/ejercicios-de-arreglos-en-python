# Identificar cual(es) son las filas de un arreglo que tienen más elementos con el valor None

a = ((5,None,7,7),(None,None,9,3),(12,4,2,0),(1,3,None,6),(0,0,None,None))

# 5    None 7    7
# None None 9    3
# 12   4    2    0
# 1    3    None 6
# 0    0    None None

cont = [0 for n in a]
f = 0
for fila in a:
    for valor in fila:
        if valor == None:
            cont[f] += 1
    f = f + 1
            
m = max(cont)
print ("Maximo:", m)
for pos in range(len(cont)):
    if cont[pos] == m:
        print ("Fila :",pos)